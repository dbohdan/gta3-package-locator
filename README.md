# _Grand Theft Auto III_ package locator

This mod adds UFO-like rotating lights around hidden packages.  It makes the packages visible from farther away.

Inspired by the [Package/Rampage Finder Mod](http://gtaforums.com/topic/203550-useful-vc-pc-mods/) for _GTA: Vice City_ by illspirit ([VC\_Lights.zip](http://www.illspirit.com/files/VC_Lights.zip)).  Coordinates from the [GTAMods Wiki](https://gtamods.com/wiki/Collectibles#GTA_III).

## Installation

1. Download the file [`generic.ide`](https://gitlab.com/dbohdan/gta3-package-locator/uploads/57f45c0c4db09c2984e30a39e0741e14/generic.ide).
2. In the directory where you installed the game go to the subdirectory `data\maps` and rename the file `generic.ide` to something like `generic.ide.original`.  Copy to this subdirectory the new `generic.ide` you have downloaded.

## Building

1. Download this repository.
2. Install Tcl 8.5 or later.  ([Magicsplat Tcl/Tk for Windows](https://www.magicsplat.com/tcl-installer/index.html) is free.  The Tcl installation that comes with Git for Windows should work, too.)
3. Place a copy of the file `data\maps\generic.ide` from an unmodified installation of GTA III in the same directory as `gen.tcl`.  Rename it to `generic.ide.original`.
4. Open the command line in the directory with `gen.tcl` and run `tclsh gen.tcl < generic.ide.original > generic.ide`.  This will create the file `generic.ide` with the mod applied.

## License

ISC.
